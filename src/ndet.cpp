#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <string>
#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <list>
#include <cassert>
#include <utility>
#include <iterator>
#include <stack>

// pour la seconde partie du projet
#include "expression_rationnelle.hpp"
#include "parser.hpp"

using namespace std;

////////////////////////////////////////////////////////////////////////////////

const unsigned int ASCII_A = 97;
const unsigned int ASCII_Z = ASCII_A + 26;
const bool         DEBUG = true;

typedef size_t                            etat_t;
typedef unsigned char                     symb_t;
typedef set< etat_t >                     etatset_t;
typedef vector< vector< etatset_t > >     trans_t;
typedef vector< etatset_t >               epsilon_t;
typedef map< etatset_t, etat_t >          map_t;

////////////////////////////////////////////////////////////////////////////////

struct sAutoNDE{
	// caractéristiques
	size_t nb_etats;
	size_t nb_symbs;
	size_t nb_finaux;

	etat_t initial;
	// état initial

	etatset_t finaux;
	// états finaux : finaux_t peut être un int*, un tableau dynamique comme vector<int>
	// ou une autre structure de donnée de votre choix.

	trans_t trans;
	// matrice de transition : trans_t peut être un int***, une structure dynamique 3D comme vector< vector< set<int> > >
	// ou une autre structure de donnée de votre choix.

	epsilon_t epsilon; 
	// transitions spontanées : epsilon_t peut être un int**, une structure dynamique 2D comme vector< set<int> >
	// ou une autre structure de donnée de votre choix.
};

////////////////////////////////////////////////////////////////////////////////

bool FromFile(sAutoNDE& at, string path){
	ifstream myfile(path.c_str(), ios::in); 
	//un flux d'entree obtenu à partir du nom du fichier
	string line;
	// un ligne lue dans le fichier avec getline(myfile,line);
	istringstream iss;
	// flux associé à la chaine, pour lire morceau par morceau avec >> (comme cin)
	etat_t s(0), t(0);
	// deux états temporaires
	symb_t a(0);
	// un symbole temporaire

	if (myfile.is_open()){
		// la première ligne donne 'nb_etats nb_symbs nb_finaux'
		do{ 
			getline(myfile,line);
		} while (line.empty() || line[0]=='#');
		// on autorise les lignes de commentaires : celles qui commencent par '#'
		iss.str(line);
		if((iss >> at.nb_etats).fail() || (iss >> at.nb_symbs).fail() || (iss >> at.nb_finaux).fail())
			return false;
		// la deuxième ligne donne l'état initial
		do{ 
			getline (myfile,line);
		} while (line.empty() || line[0]=='#');    
		iss.clear();
		iss.str(line);
		if((iss >> at.initial).fail())
			return false;

		// les autres lignes donnent les états finaux
		for(size_t i = 0; i < at.nb_finaux; i++){
			do{ 
				getline (myfile,line);
			} while (line.empty() || line[0]=='#');
			iss.clear();
			iss.str(line);
			if((iss >> s).fail())
				continue;
			//        cerr << "s= " << s << endl;
			at.finaux.insert(s);
		}     

		// on alloue les vectors à la taille connue à l'avance pour éviter les resize dynamiques
		at.epsilon.resize(at.nb_etats);
		at.trans.resize(at.nb_etats);
		for(size_t i=0;i<at.nb_etats;++i)
			at.trans[i].resize(at.nb_symbs);   

		// lecture de la relation de transition 
		while(myfile.good()){
			line.clear();
			getline (myfile,line);
			if (line.empty() && line[0]=='#')
				continue;
			iss.clear();
			iss.str(line); 

			// si une des trois lectures echoue, on passe à la suite
			if((iss >> s).fail() || (iss >> a).fail() || (iss >> t).fail() || (a< ASCII_A ) || (a> ASCII_Z ))
				continue; 

			//test espilon ou non
			if ((a - ASCII_A) >= at.nb_symbs){
				at.epsilon[s].insert(t);	
				//        cerr << "s=" << s<< ", (e), t=" << t << endl;
			}
			else{
				//        cerr << "s=" << s<< ", a=" << a-ASCII_A << ", t=" << t << endl;
				at.trans[s][a-ASCII_A].insert(t);
			}
		}
		myfile.close();
		return true; 
	}
	return false;
	// on ne peut pas ouvrir le fichier
}


// -----------------------------------------------------------------------------
// Fonctions à compléter pour la première partie du projet
// -----------------------------------------------------------------------------


bool EstDeterministe(const sAutoNDE& at){
	//s'il existe des trans. spontanées, l'automate ne peut être déterministe	

	for(unsigned int i = 0; i < at.trans.size(); ++i)
		for(size_t j = 0; j < at.nb_symbs; ++j)
			//une il doit y avoir une et une seule transition d'un état vers un autre
			if(at.trans[i][j].empty() || at.trans[i][j].size() > 1)
				return false;


	//s'il existe une transtion spontanée l'automate n'est pas déterministe
	for(size_t i = 0; i < at.nb_etats; ++i)
		if(not at.epsilon[i].empty())
			return false;

	return true;

}

////////////////////////////////////////////////////////////////////////////////

void Fermeture(const sAutoNDE& at, etatset_t& e){
	// Cette fonction clot l'ensemble d'états E={e_0, e_1, ... ,e_n} passé en
	// paramètre avec les epsilon transitions

	std::set<etat_t> e_visites; //pour le marquage des étasts visités
	std::list<etat_t> f; //file, parcours en largeur

	for(auto it = e.begin(); it != e.end(); ++it){
		f.clear();
		f.push_front(*it);

		while(not f.empty()){
			etat_t q = f.front();

			if(e_visites.find(q) == e_visites.end()){
				//si l'état en cours n'a pas déjà été traité
				for(auto it = at.epsilon[q].begin(); it != at.epsilon[q].end(); ++it)
					//on ajoute toutes les e-transitions à la file
					f.push_back(*it);

				e_visites.insert(f.front());
				e.insert(f.front());
			}
			f.pop_front();
		}
	}		
}

////////////////////////////////////////////////////////////////////////////////

etatset_t Delta(const sAutoNDE& at, const etatset_t& e, symb_t c){
	// définir cette fonction en utilisant Fermeture

	etatset_t eDelta; //ensemble des transitions par le symbole c

	etatset_t eEps; //transitions spontanées de de l'état e

	for(auto it = e.begin(); it != e.end(); ++it){
		eEps = at.trans[*it][c];
		Fermeture(at, eEps);
		eDelta.insert(eEps.begin(), eEps.end());
	}
	return eDelta;
}

////////////////////////////////////////////////////////////////////////////////

bool AcceptRec(const sAutoNDE& at, string& str, size_t iStr, etat_t q){
	if(iStr == str.size()){
		//si on est arrive au bout de la chaine
		return at.finaux.find(q) != at.finaux.cend();
	}

	else{
		char input = str[iStr];
		input -= ASCII_A;

		etatset_t trans;

		if((size_t)input >= at.nb_etats)
			return false;
		else
			trans = at.trans[q][input];

		for(auto it = trans.begin(); it != trans.end(); ++it){
			//on teste toutes les transitions possibles
			if(AcceptRec(at, str, iStr + 1, *it))
				return true;
		}

		etatset_t eps = at.epsilon[q];

		for(auto it = eps.begin(); it != eps.end(); ++it){
			//on teste toutes les e-transitions possibles
			if(AcceptRec(at, str, iStr, *it))
				return true;
		}

		//si on n'a pas de transition qui fonctionnent ou pas de transition du tout (AND)
		return false;
	}
}

bool Accept(const sAutoNDE& at, string str){
	//J'ai fait cette fonction de manière récursive, parce qu'elle s'écrit de manière
	//très "visuelle" comme ça (on passe d'un état à l'autre. Une procédure avec une pile
	//aurait été plus efficace en mémoire mais je la garde comme ça pour l'instant. Voir la
	//fonction de fermeture pour une fonction qui utilise une pile.
	
	//Appel initial pour la procédure récursive
	return AcceptRec(at, str, 0, at.initial);
}

////////////////////////////////////////////////////////////////////////////////

sAutoNDE Determinize(const sAutoNDE& at){
if(not EstDeterministe(at)){
	// si l'automate n'est pas déterministe, on le déterminise
	
	sAutoNDE r; //nouvel automate

	etatset_t etatInit = {at.initial};
	Fermeture(at, etatInit);
	std::list<etatset_t> nvEtats = {etatInit}; //file des nouveaux états	

	std::vector<etatset_t> v = {etatInit}; //liste des états trouvés
	
	for(auto itD = etatInit.begin(); itD != etatInit.end(); ++itD){
		//si on des états initiaux est final, alors le nouvel état initial sera final
		if(at.finaux.find(*itD) != at.finaux.end()){
			r.finaux.insert(0);
			break;
		}
	}

	size_t iE = 0; //indice de l'état courant pour le nouvel automate
	size_t offset = 1; //décalage pour l'insertion des nouveaux états, voir le README
										 //pour savoir pourquoi je n'ai pas utilisé d'indice

	size_t iP = 0; //id de la poubelle

	while(not nvEtats.empty()){ 
		//tant qu'il existe un état non visité
		r.trans.resize(iE + 1);
		r.trans[iE].resize(at.nb_symbs);

		for(size_t itS = 0; itS < at.nb_symbs; ++itS){
			//pour chaque symbole, vers quels états peut-on aller
			etatset_t delta = Delta(at, nvEtats.front(), itS);

			if(not delta.empty()){
				//si il existe effectivement des états pour ce symbole
				
				bool etatExiste = false;	
				for(size_t iV = 0; iV < v.size(); ++iV){
					if(v[iV] == delta){
						//l'état a déjà été rencontré avant, on connait son id
						r.trans[iE][itS].insert(iV);
						etatExiste = true;
						break;
					}
				}

				if(not etatExiste){
					//l'état est nouveau, on l'ajoute à ceux déjà trouvés
					r.trans[iE][itS].insert(iE + offset); 
					
					//On cherche si le nouvel état est final
					for(auto itD = delta.begin(); itD != delta.end(); ++itD){
						if(at.finaux.find(*itD) != at.finaux.end()){
							r.finaux.insert(iE + offset);
							break;
						}
					}
					offset++;

					v.push_back(delta);
					nvEtats.push_back(delta);
				}
			}

			else{
				//si le symbole actuel ne mène nulle part, on crée une transition
				//de l'état à la poubelle
				if(iP == 0){
					//aucune poubelle n'a été nécessaire pour l'instant, on l'insère
					//à la suite
					iP = iE + offset;

					offset++;

					etatset_t etatsP; //on crée un ensemble d'états vides uniquement
														//pour prolonger les états existants
					nvEtats.push_back(etatsP);	
				}
				r.trans[iE][itS].insert(iP);
			}	
		}
		iE++; //on passe à l'état suivant
		offset--; //par conséquent le décalage entre le nouvel et l'ancien automate
							//se réduit de 1

		nvEtats.pop_front();
	}

	//mise à jour des statistiques de l'automate
	r.nb_etats = r.trans.size();
	r.nb_symbs = at.nb_symbs;
	r.nb_finaux = r.finaux.size();

	return r;
}

return at;
}


////////////////////////////////////////////////////////////////////////////////

ostream& operator<<(ostream& out, const sAutoNDE& at){
out << at.nb_etats << ' ' << at.nb_symbs << ' ' << at.nb_finaux << endl;
out << at.initial << endl;

for(auto itF = at.finaux.begin(); itF != at.finaux.end(); ++itF)
	out << *itF << endl;

for(size_t iE = 0; iE < at.trans.size(); ++iE)
	for(size_t iS = 0; iS < at.trans[iE].size(); ++iS)
		for(auto itE = at.trans[iE][iS].begin(); itE != at.trans[iE][iS].end(); ++itE)
			out << iE << ' ' << (char)(iS + ASCII_A) << ' ' << *itE << endl;

return out;
}

////////////////////////////////////////////////////////////////////////////////

bool ToGraph(sAutoNDE& at, string path){
ofstream myfile;

myfile.open(path, ios::out);

if(myfile.is_open()){
	myfile << "digraph finite_state_machine {" << '\n';
	myfile << '\t' << "rankdir=LR;" << '\n';
	myfile << '\t' << "size=\"10,10\";" << "\n\n";

	myfile << '\t' << "node [shape = doublecircle];";

	for(auto it = at.finaux.cbegin(); it != at.finaux.cend(); ++it){
		myfile << ' ' << *it;
	}	

	if(at.nb_finaux > 0) //si par hasard il n'y avait pas d'état final
		myfile << ";" << '\n';

	myfile << "\tnode [shape = point ]; q;" << '\n';

	myfile << "\tnode [shape = circle];" << "\n\n";	

	myfile << "\tq -> " << at.initial << ";\n";

	for(unsigned int i = 0; i < at.trans.size(); ++i){
		//on enumere toutes les etats...
		for(unsigned int j = 0; j < at.trans[i].size(); ++j){
			//et toutes leurs transitions...
			for(auto it = at.trans[i][j].begin(); it != at.trans[i][j].end(); ++it){
				//et toutes les etats donnes par les transitions
				myfile << '\t' << i << " -> " << *it << " [label = \"" << (char)(j+ASCII_A) << "\"];\n";
			}
		}
	}

	myfile << '\n';

	for(unsigned int i = 0; i < at.epsilon.size(); ++i){
		for(auto it = at.epsilon[i].begin(); it != at.epsilon[i].end(); ++it){
			//on enumere toutes les e-transitions
			myfile << '\t' << i << " -> " << (*it) << " [label = \"" << "Ɛ" << "\"];\n";
		}
	}

	myfile << '\n' << '}';

}
return false;
}


// -----------------------------------------------------------------------------
// Fonctions à compléter pour la seconde partie du projet
// -----------------------------------------------------------------------------

sAutoNDE Append(const sAutoNDE& x, const sAutoNDE& y){
// fonction outil : on garde x, et on "ajoute" trans et epsilon de y
// en renommant ses états, id est en décallant les indices des états de y
// de x.nb_etats 
	assert(x.nb_symbs == y.nb_symbs);
	sAutoNDE r;

	r.nb_symbs = x.nb_symbs;
	r.nb_finaux = x.nb_finaux + y.nb_finaux;
	r.nb_etats = x.nb_etats + y.nb_etats;
	r.initial = x.initial;

	r.trans = x.trans;
	r.finaux = x.finaux;
	r.epsilon = x.epsilon;

	//allocation pour contenir tous les nouveaux états
	r.trans.resize(r.nb_etats);
	for(size_t i = 0; i < r.nb_etats; ++i){
		r.trans[i].resize(r.nb_symbs);
	}

	//on ajoute les transitions de y en prenant soin de décaler les numéros des états
	for(size_t i = 0; i < y.nb_etats; ++i){
		for(size_t j = 0; j < y.nb_symbs; ++j){
			for(auto it = y.trans[i][j].begin(); it != y.trans[i][j].end(); ++it)
				r.trans[i+x.nb_etats][j].insert(*it + x.nb_etats);
		}
	}

	//allocation pour contenir tous les nouveaux états
	r.epsilon.resize(r.nb_etats);

	//mise à jour des transitions
	for(size_t i = 0; i < y.nb_etats; ++i){
		for(auto it = y.epsilon[i].begin(); it != y.epsilon[i].end(); ++it)
			r.epsilon[i + x.nb_etats].insert(*it + x.nb_etats);
	}

	//mise à jour des transitions spontanées
	for(auto it = y.finaux.begin(); it != y.finaux.end(); ++it)
		r.finaux.insert(*it + x.nb_etats);

	return r;
}

////////////////////////////////////////////////////////////////////////////////

sAutoNDE Union(const sAutoNDE& x, const sAutoNDE& y){
	assert(x.nb_symbs == y.nb_symbs);
	sAutoNDE r = Append(x, y);

	//un état supplémentaire (le nouvel état initial) est ajouté
	r.nb_etats++;

	//réallocation des transitions pour qu'elles aient la bonne dimension
	r.trans.resize(r.nb_etats);
	for(size_t i = 0; i < r.nb_etats; ++i)
		r.trans[i].resize(r.nb_symbs);

	//réallocation des transitions spontanées pour qu'elles aient la bonne dimension
	r.epsilon.resize(r.nb_etats);

	//mise à jour du nouvel état initial
	r.initial = r.nb_etats - 1;

	//on ajoute une transition du nouvel état initial vers les états initiaux
	//des précédents automates
	r.epsilon[r.initial].insert(x.initial);
	r.epsilon[r.initial].insert(x.nb_etats + y.initial);

	return r;
}

////////////////////////////////////////////////////////////////////////////////

sAutoNDE Concat(const sAutoNDE& x, const sAutoNDE& y){
	assert(x.nb_symbs == y.nb_symbs);
	sAutoNDE r = Append(x, y);

	etatset_t nouveauFinaux;
	nouveauFinaux.insert(r.nb_etats - 1);

	r.finaux = nouveauFinaux;

	//r.epsilon[x.nb_etats-1].insert(x.nb_etats);

	for(auto it = x.finaux.begin(); it != x.finaux.end(); ++it){
		r.epsilon[*it].insert(x.nb_etats + y.initial);
	}

	return r;
}

////////////////////////////////////////////////////////////////////////////////

sAutoNDE Complement(const sAutoNDE& x){


	return x;
}

////////////////////////////////////////////////////////////////////////////////

sAutoNDE Kleene(const sAutoNDE& x){
	sAutoNDE r;

	//on rajoute un état qui sera l'état initial et sera également final
	r.nb_etats = x.nb_etats + 1;
	r.nb_symbs = x.nb_symbs;
	r.nb_finaux = x.nb_finaux + 1;

	r.trans = x.trans;
	r.epsilon = x.epsilon;
	r.finaux = x.finaux;
	r.initial = x.initial;

	//réallocation des transitions pour avoir les bonnes dimensions
	r.trans.resize(r.nb_etats);
	for(size_t i = 0; i < r.nb_etats; ++i){
		r.trans[i].resize(r.nb_etats);
	}	

	//réallocation des transitions spontanées pour avoir les bonnes dimensions
	r.epsilon.resize(r.nb_etats);

	//on ajoute une transition entre le nouvel état initial et l'ancien état initial
	r.epsilon[r.nb_etats - 1].insert(r.initial);

	//pour chaque état état final on ajoute une transition vers l'ancien état initial
	for(auto it = r.finaux.begin(); it != r.finaux.end(); ++it)
		r.epsilon[*it].insert(r.initial);

	//mise à jour du nouvel état initial, qui est également final (mot vide)
	r.initial = r.nb_etats - 1;
	r.finaux.insert(r.nb_etats - 1);

	return r;
}

////////////////////////////////////////////////////////////////////////////////

sAutoNDE Intersection(const sAutoNDE& x, const sAutoNDE& y){
//TODO définir cette fonction

return x;

}

////////////////////////////////////////////////////////////////////////////////

void egaliseNbSymboles(sAutoNDE & aut1, sAutoNDE & aut2){
	if(aut1.nb_symbs < aut2.nb_symbs){
		for(size_t i = 0; i < aut1.nb_etats; ++i){
			aut1.trans[i].resize(aut2.nb_symbs);
		}

		aut1.nb_symbs = aut2.nb_symbs;
	}
	else if(aut1.nb_symbs > aut2.nb_symbs)
		egaliseNbSymboles(aut2, aut1);
}

////////////////////////////////////////////////////////////////////////////////

sAutoNDE ExpressionRationnelle2Automate(string expr){
	cout << "Construction d'un automate à partir d'une expression rationnelle\n";
	cout << "  Expression en entrée (string) : " << expr << endl;

	sExpressionRationnelle er = lit_expression_rationnelle(expr);

	cout << "  Expression en entrée (ASA)    : " << er << endl;

	//les expressions à traiter seront stockées dans une pile
	stack< sExpressionRationnelle > sr;
	sr.push(er);

	//on stocke les automates correspondant à une expression dans une map
	map<sExpressionRationnelle, sAutoNDE> autByExpr;	
	
	sExpressionRationnelle exprActuelle;
	while(not sr.empty()){
		exprActuelle = sr.top();
		//si l'expression actuelle a déjà été trouvée
		if(autByExpr.find(exprActuelle) != autByExpr.end()){
			sr.pop();
		}	

		else{
			sAutoNDE aut;
			//si l'expression trouvée est une variable
			if(exprActuelle -> op == o_variable){
				//construction de l'automate : 2 états, et une transition entre l'état 0
				//et l'état 1
				aut.nb_symbs = *exprActuelle -> nom -> begin() - ASCII_A + 1;
				aut.nb_etats = 2;
				aut.nb_finaux = 1;

				aut.finaux.insert(1);
				
				//allocation des relations
				aut.epsilon.resize(aut.nb_etats);
				aut.trans.resize(aut.nb_etats);
				for(size_t i = 0; i < aut.nb_etats; ++i){
					aut.trans[i].resize(aut.nb_symbs);
				}
				
				aut.initial = 0;
				aut.trans[0][aut.nb_symbs - 1].insert(1);	

				autByExpr[exprActuelle] = aut;
				sr.pop();
			}

			//si l'expression trouvée est une étoile
			else if(exprActuelle -> op == o_etoile){
				//si l'expression dans l'étoile est inconnue, on l'ajoute à la pile
				if(autByExpr.find(exprActuelle -> arg) != autByExpr.end()){
					aut = Kleene(autByExpr[exprActuelle -> arg]);
					autByExpr[exprActuelle] = aut;
					sr.pop();
				}
				//sinon on la traite et on supprime l'étoile de la pile
				else{
					sr.push(exprActuelle -> arg);
				}
			}
			//si l'expression trouvée est une une relation binaire
			else{
				//si le ou a ses deux arguments déterminés, on le traite
				if(autByExpr.find(exprActuelle -> arg1) != autByExpr.end() && autByExpr.find(exprActuelle -> arg2) != autByExpr.end()){
					//on ajuste la taille des automates
					egaliseNbSymboles(autByExpr[exprActuelle -> arg1], autByExpr[exprActuelle -> arg2]);

					//selon le type d'expression on construit l'automate correspondant
					switch(exprActuelle -> op){
						case o_concat:
							aut = Concat(autByExpr[exprActuelle -> arg1], autByExpr[exprActuelle -> arg2]);
							break;
						case o_ou:
							aut = Union(autByExpr[exprActuelle -> arg1], autByExpr[exprActuelle -> arg2]);
							break;
						default:
							break;
					}

					autByExpr[exprActuelle] = aut;
					sr.pop();
				}
				
				//sinon on les ajoute à la pile et on les traite plus tard
				else{
					if(autByExpr.find(exprActuelle -> arg1) == autByExpr.end())
						sr.push(exprActuelle -> arg1);
					if(autByExpr.find(exprActuelle -> arg2) == autByExpr.end())
						sr.push(exprActuelle -> arg2);
				}
			}
		}
	}

	return autByExpr[er];
}

////////////////////////////////////////////////////////////////////////////////

sAutoNDE arrangeAutomate(sAutoNDE at, sAutoNDE &atA){
	//il y a deux états supplémentaires après la transformation
	atA.nb_etats = at.nb_etats + 2;
	atA.epsilon.resize(at.nb_etats + 2);

	//le premier élément est l'état initial, les suivant sont décalés de 1 vers la droite
	//et le dernier est l'état final, qui n'a aucune transition
	atA.epsilon[0] = {at.initial + 1};
	for(size_t i = 0; i < at.epsilon.size(); ++i){	
		for(auto it = at.epsilon[i].begin(); it != at.epsilon[i].end(); ++it){
			atA.epsilon[i + 1].insert(*it + 1);
		}
	}

	//il n'y a plus qu'un seul état final
	atA.nb_finaux = 1;

	//on crée un e-transition des anciens états finaux vers l'unique du nouvel automate
	atA.initial = 0;
	atA.finaux.insert(atA.nb_etats - 1);
	for(auto it = at.finaux.begin(); it != at.finaux.end();  ++it){
		atA.epsilon[*it + 1].insert(at.nb_etats + 1);
	}
	
	//il y a n + 2 états, et le nombre de symboles ne change pas
	atA.trans.resize(at.nb_etats + 2);
	atA.nb_symbs = at.nb_symbs;
	
	for(size_t i = 0; i < atA.nb_etats; ++i){
		atA.trans[i].resize(atA.nb_symbs);
	}

	//l'état initial ne contient qu'une transition spontanée, puis on ajoute toutes les anciennes
	//transitions, puis l'état final ne contient qu'une transition spontanée
	for(size_t i = 0; i < at.nb_etats; ++i){
		for(size_t j = 0; j < at.nb_symbs; ++j){
			for(auto it = at.trans[i][j].begin(); it != at.trans[i][j].end(); ++it){
				atA.trans[i + 1][j].insert(*it + 1);
			}
		}
	}

	return atA;
}

////////////////////////////////////////////////////////////////////////////////

string Automate2ExpressionRationnelle(sAutoNDE at){
	cout << "Construction d'une expression rationnelle à partir d'un automate\n";

	string sr;
	sAutoNDE atA;
	arrangeAutomate(at, atA);

	//on utilise un tableau à 3 dimensions pour stocker les expressions intermédiaires
	std::vector < std::vector <std::vector< sExpressionRationnelle >>>R;

	//allocation du tableau pour stocker les transitions
	R.resize(atA.nb_etats);
	for(size_t i = 0; i < atA.nb_etats; ++i){
		R[i].resize(atA.nb_etats);
		for(size_t j = 0; j < atA.nb_etats; ++j){
			R[i][j].resize(atA.nb_etats, nullptr);
		}
	}

	//correspondance variables -> ASCII
	std::vector< string > corres(atA.nb_symbs + 1);
	for(size_t i = 0; i < atA.nb_symbs; ++i){
		corres[i] += (i + ASCII_A);
	}

	//la dernière valeur du tableau correspond aux transitions spontanées, e si les symboles sont tous
	//inférieurs à e, et le symbole immédiatement supérieur au plus grand symbole sinon
	corres[atA.nb_symbs] = (atA.nb_symbs - 1 + ASCII_A < 'e' ? 'e' : atA.nb_etats + ASCII_A);

	//calcul des R(i, j, 0) pour les transitions
	for(size_t i = 0; i < atA.nb_etats; ++i){
		for(size_t j = 0; j < atA.nb_etats; ++j){
			for(size_t k = 0; k < atA.nb_symbs; ++k){
				auto trans = atA.trans[i][k].find(j);
				if(trans != atA.trans[i][k].end()){	
					sExpressionRationnelle var = variable(corres[k]);
					if(R[i][j][0] != nullptr)
						R[i][j][0] = ou(R[i][j][0], var);
					else
						R[i][j][0] = var;
				}
			}
		}
	}

	//calcul des R(i, j, 0) pour les transitions spontanées
	for(size_t i = 0; i < atA.nb_etats; ++i){
		for(auto it = atA.epsilon[i].begin(); it != atA.epsilon[i].end(); ++it){
			sExpressionRationnelle var = variable(corres[atA.nb_symbs]);
			if(R[i][*it][0] != nullptr)
				R[i][*it][0] = ou(R[i][*it][0], var);
			else
				R[i][*it][0] = var;
		}
	}	

	//calcul des R(i, j, n) en fonction de R(i, j, n-1)
	for(size_t n = 1; n < atA.nb_etats; ++n){
		//on parcourt tous les i, possibles
		for(size_t i = 0; i < atA.nb_etats; ++i){
			for(size_t j = 0; j < atA.nb_etats; ++j){
				//l'état n va disparaitre, on ne calcule donc pas les transitions d'un état vers n
				if(n != i && n != j){
					//ajout de la flèche entre i et j
					if(R[i][j][n-1] != nullptr)
						R[i][j][n] = R[i][j][n-1];

					sExpressionRationnelle flecheInter = nullptr;

					//si il existe un chemin entre i et j qui passe par n, on le calcule
					if(R[i][n][n-1] != nullptr && R[n][j][n-1] != nullptr){
						//si la transition entre i et j n'est pas une transition spontanée, on l'ajoute
						if(*R[i][n][n-1] -> nom != corres[atA.nb_symbs])
							flecheInter = R[i][n][n-1];

						//ajout de la boucle sur n
						if(R[n][n][n-1] != nullptr){
							if(flecheInter != nullptr)
								flecheInter = concat(flecheInter, etoile(R[n][n][n-1]));
							else
								flecheInter = etoile(R[n][n][n-1]);
						}

						//ajout de la flèche entre n et j
						if(*R[n][j][n-1] -> nom != corres[atA.nb_symbs]){
							if(flecheInter != nullptr)
								flecheInter = concat(flecheInter, R[n][j][n-1]);
							else 
								flecheInter = R[n][j][n-1];
						}

						//si la flèche entre i et j qui passe par n est vide, c'est qu'il s'agit d'une transition
						//spontanée
						if(flecheInter == nullptr)
							flecheInter = variable(corres[atA.nb_symbs]);
					}

					//si il existe une flèche entre i et j directe et une passant par n, on en fait l'union
					if(flecheInter != nullptr && R[i][j][n] != nullptr){
						if(*R[i][j][n] -> nom != *flecheInter->nom)
							R[i][j][n] = ou(R[i][j][n], flecheInter);
					}
					//si il n'existe pas de flèche directe, seule la flèche qui passe par n est ajoutée
					else if(flecheInter != nullptr)
						R[i][j][n] = flecheInter;
				}
			}
		}
	}

	//l'expression correspondant à l'automate est l'expression entre 0 et l'état final
	sr = expression_rationnelle2string(R[0][atA.nb_etats-1][atA.nb_etats-2]);

	//libération de la mémoire
	free_all(R[0][at.nb_etats-1][atA.nb_etats-2]);

	return sr;
}

////////////////////////////////////////////////////////////////////////////////

void genereMots(string strBase, vector<string> & mots, size_t & nb_symbs, size_t & tailleMax){
	if(strBase.size() < tailleMax){
		for(size_t i = 0; i < nb_symbs; ++i){
			mots.push_back(strBase + (char)(i + ASCII_A));	
			genereMots(strBase + (char)(i + ASCII_A), mots, nb_symbs, tailleMax);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////

bool Equivalent(const sAutoNDE& a1, const sAutoNDE& a2) {
	size_t tailleMotMax = max(a1.nb_etats, a2.nb_etats);
	size_t nbSymbsMax = max(a1.nb_symbs, a2.nb_symbs);

	vector<string> mots;

	//génération des dont la longueur est inférieure au nombre d'états de l'automate
	//sur l'ensemble des symboles de l'automate
	genereMots("", mots, nbSymbsMax, tailleMotMax);

	//pour chaque mot on vérifie s'il est accepté par les deux automates
	for(size_t i = 0; i < mots.size(); ++i){
		if((Accept(a1, mots[i]) && !Accept(a2, mots[i])) || (!Accept(a1, mots[i]) && Accept(a2, mots[i]))){
			return false;
		}
	}	

	return true;
}

////////////////////////////////////////////////////////////////////////////////

void Help(ostream& out, char *s){
	out << "Utilisation du programme " << s << " :" << endl ;
	out << "-acc ou -accept Input Word :\n\t détermine si le mot Word est accepté par l'automate Input" << endl;
	out << "-det ou -determinize Input Output [-g] :\n\t déterminise l'automate Input, écrit le résultat dans Output" << endl;
	out << "-isdet ou -is_deterministic Input :\n\t détermine si l'automate Input est déterministe" << endl;
	out << "-aut2expr ou automate2expressionrationnelle Input :\n\t calcule l'expression rationnelle correspondant à l'automate Input et l'affiche sur la sortie standard" << endl;
	out << "-expr2aut ou expressionrationnelle2automate ExpressionRationnelle Output [-g] :\n\t calcule l'automate correspondant à ExpressionRationnelle, écrit l'automate résultant dans Output" << endl;
	out << "-equ ou -equivalent Input1 Intput2 :\n\t détermine si les deux automates Input1 et Input2 sont équivalents" << endl;
	out << "-nop ou -no_operation Input Output [-g] :\n\t ne fait rien de particulier, recopie l'entrée dans Output" << endl;

	out << "Exemple '" << s << " -determinize auto.txt resultat -g" << endl;
}

////////////////////////////////////////////////////////////////////////////////

int main(int argc, char* argv[] ){
	sAutoNDE at;

	if(argc < 3){
		Help(cout, argv[0]);
		return EXIT_FAILURE;
	}

	int pos;
	int act=-1;                 // pos et act pour savoir quelle action effectuer
	int nb_ifiles = 0;          // nombre de fichiers en entrée
	int nb_ofiles = 0;          // nombre de fichiers en sortie
	string str, in1, in2, out, acc, expr;
	// chaines pour (resp.) tampon; fichier d'entrée Input1; fichier d'entrée Input2;
	// fichier de sortie et chaine dont l'acceptation est à tester 
	bool graphMode=false;     // sortie graphviz ?

	// options acceptées
	const size_t NBOPT = 8;
	string aLN[] = {"accept", "determinize", "is_terministic", "automate2expressionrationnelle", "expressionrationnelle2automate", "equivalent", "no_operation", "graph"};
	string aSN[] = {"acc", "det", "isdet", "aut2expr", "expr2aut", "equ", "nop", "g"};

	// on essaie de "parser" chaque option de la ligne de commande
	for(int i=1; i<argc; ++i){
		if (DEBUG) cerr << "argv[" << i << "] = '" << argv[i] << "'" << endl;
		str = argv[i];
		pos = -1;
		string* pL = find(aLN, aLN+NBOPT, str.substr(1));
		string* pS = find(aSN, aSN+NBOPT, str.substr(1));

		if(pL!=aLN+NBOPT)
			pos = pL - aLN;
		if(pS!=aSN+NBOPT)
			pos = pS - aSN;   

		if(pos != -1){
			// (pos != -1) <=> on a trouvé une option longue ou courte
			if (DEBUG) cerr << "Key found (" << pos << ") : " << str << endl;
			switch (pos) {
				case 0: //acc
					in1 = argv[++i];
					acc = argv[++i];
					nb_ifiles = 1;
					nb_ofiles = 0;
					break;
				case 1: //det
					in1 = argv[++i];
					out = argv[++i];
					nb_ifiles = 1;
					nb_ofiles = 1;
					break;
				case 2: //isdet
					in1 = argv[++i];
					nb_ifiles = 1;
					nb_ofiles = 0;
					break;
				case 3: //aut2expr
					in1 = argv[++i];
					nb_ifiles = 1;
					nb_ofiles = 0;
					break;
				case 4: //expr2aut
					expr = argv[++i];
					out = argv[++i];
					nb_ifiles = 0;
					nb_ofiles = 1;
					break;
				case 5: //equ
					in1 = argv[++i];
					in2 = argv[++i];
					nb_ifiles = 2;
					nb_ofiles = 0;
					break;
				case 6: //nop
					in1 = argv[++i];
					out = argv[++i];
					nb_ifiles = 1;
					nb_ofiles = 1;
					break;          
				case 7: //g
					graphMode = true;
					break;
				default:
					return EXIT_FAILURE;
			}
		}
		else{
			cerr << "Option inconnue "<< str << endl;
			return EXIT_FAILURE;
		}

		if(pos<7){
			if(act > -1){
				cerr << "Plusieurs actions spécififées"<< endl;
				return EXIT_FAILURE;
			}
			else
				act = pos;
		}    
	}

	if (act == -1){
		cerr << "Pas d'action spécififée"<< endl;
		return EXIT_FAILURE;
	}  

	/* Les options sont OK, on va essayer de lire le(s) automate(s) at1 (et at2)
		 et effectuer l'action spécifiée. Atr stockera le résultat*/

	sAutoNDE at1, at2, atr;

	// lecture du des fichiers en entrée
	if ((nb_ifiles == 1 or nb_ifiles == 2) and !FromFile(at1, in1)){
		cerr << "Erreur de lecture " << in1 << endl;
		return EXIT_FAILURE;
	}  
	if (nb_ifiles ==2 and !FromFile(at2, in2)){
		cerr << "Erreur de lecture " << in2 << endl;
		return EXIT_FAILURE;
	}  

	switch(act) {
		case 0: //acc
			if (Accept(at1, acc)){
				cout << "'" << acc << "' est accepté : OUI\n";
			}
			else {
				cout << "'" << acc << "' est accepté : NON\n";
			}
			break;
		case 1: //det
			atr = Determinize(at1);
			break;
		case 2: //isdet
			if (EstDeterministe(at1)){
				cout << "l'automate fourni en entrée est déterministe : OUI\n";
			}
			else {
				cout << "l'automate fourni en entrée est déterministe : NON\n";
			}
			break;
		case 3: //aut2expr
			expr =  Automate2ExpressionRationnelle(at1);
			cout << "Expression rationnelle résultante :" << endl << expr << endl;
			break;
		case 4: //expr2aut
			atr =  ExpressionRationnelle2Automate(expr);
			break;
		case 5: //equ
			if (Equivalent(at1,at2)){
				cout << "les deux automates sont équivalents : OUI\n";
			}
			else {
				cout << "les deux automates sont équivalents : NON\n";
			}
			break;
		case 6: //nop
			atr = at1;
			break;
		default:
			return EXIT_FAILURE;
	}

	if (nb_ofiles == 1){
		// on affiche le résultat
		// cout << "Automate résultat :\n----------------\n";
		// cout << atr;

		// écriture dans un fichier texte
		ofstream f((out + ".txt").c_str(), ios::trunc); 
		if(f.fail())
			return EXIT_FAILURE;
		f << atr;    

		// génération d'un fichier graphviz
		if(graphMode){
			ToGraph(atr, out + ".gv");
			system(("dot -Tpng " + out + ".gv -o " + out + ".png").c_str());
		}
	}

	return EXIT_SUCCESS;
}



